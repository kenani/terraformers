resource "aws_instance" "web" {
  ami = "ami-0fa49cc9dc8d62c84"
  instance_type = "t2.micro"
  key_name = "day5_947670"

  provisioner "remote-exec" {
    inline = [
	"sudo yum install git -y",
	"sudo yum install tree -y"
    ]
  }
  connection {
    type = "ssh"
    host = self.public_ip
    user = "ec2-user"
    private_key = "${file("./day5_947670.pem")}"
  }

}


