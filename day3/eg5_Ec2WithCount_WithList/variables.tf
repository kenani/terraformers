variable "lab_name"{
        default = "CCL"
        description = "provide your lab name"
}

variable "emp_id"{
        default = "947670"
        description = "provide employee id"
}


variable "ami_id"{
	default = ["ami-0fa49cc9dc8d62c84","ami-0aeb7c931a5a61206","ami-0323a02a58ca8ce7f"]
	description = "provide ami id as per your region"
}

variable "instance_type"{
	default = "t2.micro"
	description = "provide instance type"
}

variable "name"{
	default = "terraformers"
	description = "provide instance name"
}
