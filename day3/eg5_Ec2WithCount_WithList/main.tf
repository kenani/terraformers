locals {
 suffix= "${var.lab_name}-${var.emp_id}"
}


resource "aws_instance" "instance"{

    ami = var.ami_id[count.index]
    instance_type = var.instance_type
    count=length(var.ami_id)
 
   tags = {
    Project="${local.suffix}-${var.name}"
    
    }
}
