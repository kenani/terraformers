locals {
  
 suffix= "${var.lab_name}-${var.emp_id}"

}



resource "aws_instance" "instance"{

    ami = var.ami_id
    instance_type = var.instance_type
 
   tags = {
    Project="${local.suffix}-${var.name}"
    
    }
}
