locals {
  
 suffix= "${var.lab_name}-${var.emp_id}"

}



resource "aws_instance" "instance"{
    
    for_each = var.ami_id
    ami = each.value
    instance_type = var.instance_type
 
   tags = {
    Project="${local.suffix}-${var.name}"
    
    }
}
