 terraform {
      required_providers {
        aws = {
	     source = "hashicorp/aws"
         version = "3.50.0"
	      }
      }
 }

 provider "aws" {
	region = "us-east-1"
        shared_credentials_file = "~/.aws/credentials"
        assume_role {
           role_arn = "arn:aws:iam::202018276677:role/terraformers-admin-role" # replace this role arn with yours
           session_name ="terraformers_session"
    }
 }
