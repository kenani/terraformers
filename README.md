# terraFORmers



## Getting started

```
cd existing_repo
git remote add origin https://gitlab.com/kenani/terraformers.git
git branch -M main
git push -uf origin main
```
## login to the instance
1) open cmd prompt
2) cd Downloads
3) ssh -i your.pem ec2-user@1.2.3.4

## Installing on linux: 

```
#https://releases.hashicorp.com/terraform
wget https://releases.hashicorp.com/terraform/0.15.3/terraform_0.15.3_linux_amd64.zip 	
unzip terraform_0.15.3_linux_amd64.zip
rm -rf terraform_0.15.3_linux_amd64.zip
chmod +x terraform
mv terraform /sbin
terraform -help
```
