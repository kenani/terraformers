terraform {
  backend "s3" {
	region = "us-east-2"
	bucket = "terraformers947670/state/"
	key  = "terraformers.tfstate"
	dynamodb_table = "terraformers947670"
  }
}

