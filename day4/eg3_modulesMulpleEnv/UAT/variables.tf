variable "env"{
        default = "UAT"
        description = "provide your environment"
}

variable "ami_id"{
	default = "ami-0fa49cc9dc8d62c84"
	description = "provide ami id as per your region"
}

variable "instance_type"{
	default = "t2.micro"
	description = "provide instance type"
}

variable "project"{
	default = "terraformers"
	description = "provide instance name"
}
