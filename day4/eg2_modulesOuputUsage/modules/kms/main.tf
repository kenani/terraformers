resource "aws_kms_key" "key" {
  description             = "KMS for ${var.name}"
}

resource "aws_kms_alias" "a" {
  name          = "alias/${var.name}"
  target_key_id = aws_kms_key.key.key_id
}
