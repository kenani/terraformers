resource "local_file" "file" {
    content  = var.content
    filename = var.filename

depends_on = [random_pet.pet]
}


resource "random_pet" "pet" {
 prefix = var.prefix
 separator = var.separator
 length = var.length
}

output "PetId" {
  value = random_pet.pet.id
}

