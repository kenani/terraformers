variable "filename" {
default="terraformer"

}


variable "terraformers" {
 type = object({
        name = string
        total = number
        location = list(string)
        haslab = bool
 })
 default = {

       name = "Terraformers"
       total = 25
       location = ["Hyderabad", "Bengaluru", "Tamilnadu"]
       haslab= true

 }

}
