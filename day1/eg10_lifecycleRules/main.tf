resource "local_file" "file" {
    content  = var.content
    filename = var.filename

 lifecycle {
   create_before_destroy=true
 }
}


output "PetId" {
  value = local_file.file.content
}

