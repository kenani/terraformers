
resource "random_pet" "server" {
 prefix = var.prefix
 separator = var.separator
 length = var.length
}

resource "local_file" "file" {
    content  = random_pet.server.id
    filename = var.filename
}
