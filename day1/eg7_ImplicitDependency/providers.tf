terraform {
  required_providers {
    local = {
      source = "hashicorp/local"
    }
    random = { 
      source = "hashicorp/random"
    }
  }
}

provider "local" {
  # Configuration options
}

provider "random" {
  # Configuration options
}
