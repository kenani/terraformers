variable "filename" {
 default =""
 type=string
 description="filename"
}

variable "content" {
 default =""
 type=string
 description="content"
}

variable "prefix" {
 default =""
 type=string
 description="prefix"
}

variable "separator" {
 default =""
 type=string
 description="seperator"
}

variable "length" {
 type=number
 description="length"
}

