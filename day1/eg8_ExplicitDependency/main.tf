resource "local_file" "file" {
    content  = var.content
    filename = var.filename

depends_on = [random_pet.server]
}


resource "random_pet" "server" {
 prefix = var.prefix
 separator = var.separator
 length = var.length
}

